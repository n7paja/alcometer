import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {
  weight: number;
  gender: string;
  time_input: number;
  bottle_input: number;
  genders = [];
  time = [];
  bottles = [];
  promilles: number;

  constructor() {}

  ngOnInit() {
    this.genders.push('Male');
    this.genders.push('Female');

    this.time = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24];
  
    this.bottles = [1,2,3,4,5,6,7,8,9,10];
  }

  calculate() {
    let litres = this.bottle_input * 0.33;
    let grams = litres * 8 * 4.5;

    let burning = this.weight / 10;

    let grams_left = grams - (burning * this.time_input);

    if(this.gender === 'Male') {
      this.promilles = grams / (this.weight * 0.7);
    } else {
      this.promilles = grams / (this.weight * 0.6);
    }
  }
}